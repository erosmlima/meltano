---
sidebarDepth: 2
---

# Team Handbook

The Meltano team handbook is the central repository for how the team functions and builds the product.

As Meltano is part of [GitLab](https://about.gitlab.com/), those parts of the [GitLab team handbook](https://about.gitlab.com/handbook/) that are not specific to the GitLab product can be assumed to also apply to Meltano.

## Subresources

- [Engineering Handbook](/handbook/engineering/)
- [Marketing Handbook](/handbook/marketing/)
- [Product Handbook](/handbook/product/)
- [Data Learning Resources](/handbook/resources/)

## Team

- [Douwe Maan](https://about.gitlab.com/company/team/#DouweM) - General Manager
- [Taylor Murphy](https://about.gitlab.com/company/team/#tayloramurphy) - Lead
- [AJ Steers](https://about.gitlab.com/company/team/#asteers) - Lead

## Customer Support

### Email

Emails to `hello@meltano.com` forward to Zendesk so they can be triaged, assigned, and managed.
This email is not advertised anywhere (but here), but is used for all of our subscriptions.
Team members have their own accounts and master account login info can be found in 1Password.

#### Responsible Disclosure of Security Vulnerabilities

Emails to `security@meltano.com` also forward to Zendesk, and are automatically assigned to the Security group, which includes all current team members.
As documented in our [Responsible Disclosure Policy](/docs/responsible-disclosure.md), we will acknowledge receipt of a vulnerability report the next business day and strive to send the reporter regular updates about our progress.

### Intercom Live Chat powered

The [Meltano.com website](https://www.meltano.com) is set up with live chat powered by Intercom.
Team members have their own accounts and master account login info can be found in 1Password.

## 1Password

We have three vaults in GitLab's 1Password account:

- Meltano: Credentials for services, typically registered to `hello@meltano.com`
- Meltano Test Data: Test data for sources
- MeltanoData.com: Credentials for VMs we created on DigitalOcean for MeltanoData.com users

## Calendars

We have email addresses and Google Calendar accounts both under `@gitlab.com` and `@meltano.com`,
but prefer to use the latter for Meltano-specific meetings.

If team members should show up as busy on their GitLab calendars during these slots to prevent other meetings from being planned over them,
these accounts can be invited to the event _in addition to_ the Meltano accounts.

Shared calendars and meetings:
- [Community Meetings](https://calendar.google.com/calendar/u/1?cid=Y18wMWNqNDhoYTRoMTk5Y3RqZWZpODV0OWRnY0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t): Meetings open to the community
  - [Office Hours](/handbook/engineering/#office-hours): Weekly on Wednesday, owned by AJ
  - [Demo Day](/handbook/engineering/#demo-day): Weekly on Friday, owned by Taylor
- Team Meetings: Meetings with the team
  - Kick off: Weekly on Monday, owned by Douwe
- Time Off: All-day events for periods team members will not be working

## Trademarks

Meltano is a registered trademark of GitLab B.V. in the USA and China.

Further details, such as the registration number and certificate, can be found in the "Meltano" 1Password vault.
